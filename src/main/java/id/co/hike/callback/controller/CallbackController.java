package id.co.hike.callback.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.hike.callback.model.Callback;

@RestController
public class CallbackController {
	
	@Produce(uri = "direct:callback")
	private ProducerTemplate callback;
	
	private final ObjectMapper obj = new ObjectMapper();
	
	@RequestMapping(value = "/callback/komodo", method = RequestMethod.GET)
	@ResponseBody
	public String callbackKomodo(@RequestParam String request_id,
			@RequestParam String transaction_id,
			@RequestParam String transaction_date,
			@RequestParam String store_name,
			@RequestParam String terminal_name,
			@RequestParam String product_name,
			@RequestParam String destination,
			@RequestParam String rc,
			@RequestParam String message,
			@RequestParam String sn,
			@RequestParam String amount,
			@RequestParam String ending_balance) throws InterruptedException, ExecutionException, JsonProcessingException {
		Callback c = new Callback();
		c.setId(request_id);
		if(rc.equals("00")) {
			c.setStatus("Sukses");
			if(product_name.startsWith("PLN")) {
				Map<String,String> info = new HashMap<String,String>();
				info.put("Stroom/Token", sn.substring(sn.indexOf("Token")+1,sn.length()));
				info.put("No Pelanggan", destination);
				info.put("Nama Pelanggan", sn.substring(0, sn.indexOf("kWh")));
				
				c.setInfo(obj.writeValueAsString(info));
			}
		}
		else
			c.setStatus("Gagal");
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("request_id",request_id);
		map.put("transaction_id",transaction_id);
		map.put("transaction_date",transaction_date);
		map.put("store_name",store_name);
		map.put("terminal_name",terminal_name);
		map.put("product_name",product_name);
		map.put("destination",destination);
		map.put("rc",rc);
		map.put("message",message);
		map.put("sn",sn);
		map.put("amount",amount);
		map.put("ending_balance",ending_balance);
		
		c.setMessage(obj.writeValueAsString(map));
		
		Object msg = callback.requestBody(callback.getDefaultEndpoint(), c);
		
		return msg.toString();
	}
	
	
}
