package id.co.hike.callback.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import id.co.hike.callback.processor.CallbackProcessor;

@Component
public class CallbackRoute extends RouteBuilder {

	@Value("${gateway.fspadapter.host}") 
	private String fsp;
	
	@Override
	public void configure() throws Exception {
		
		from("direct:callback").id("callback")
		.process(new CallbackProcessor())
		.toD("http://"+fsp+":8011/fsp/update_history?id=${header.id}&status=${header.status}&message=${header.message}&info=${header.info}")
		.convertBodyTo(String.class).end();
	}
}