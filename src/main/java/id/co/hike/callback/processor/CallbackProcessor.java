package id.co.hike.callback.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import id.co.hike.callback.model.Callback;

public class CallbackProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		Callback c = exchange.getIn().getBody(Callback.class);
		
		exchange.getOut().setHeader("id", c.getId());
		exchange.getOut().setHeader("status", c.getStatus());
		exchange.getOut().setHeader("message", c.getMessage());
		exchange.getOut().setHeader("info", c.getInfo());
		
	}

}
