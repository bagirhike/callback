FROM maven:3.5.2-jdk-8-alpine AS MAVEN_BUILD

MAINTAINER Muhammad Bagir Alaydrus

COPY pom.xml /build/

COPY src /build/src/

WORKDIR /build/

RUN mvn package

FROM openjdk:8-jre-alpine

WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/gateway-callback.war /app/

ENTRYPOINT ["java", "-jar", "gateway-callback.war"]
